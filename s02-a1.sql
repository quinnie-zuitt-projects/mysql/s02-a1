
CREATE DATABASE blog_db;

CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(300) NOT NULL,
  datetime_created DATETIME
  PRIMARY KEY (id)

);

CREATE TABLE posts (
	id INT 
	author_id INT,
	title VARCHAR(500),
	content VARCHAR(5000),
	datetime_posted DATETIME
	PRIMARY KEY (id)
	FOREIGN KEY  (author_id) REFERENCES users(id)
);

CREATE TABLE post_Comments(
	id INT,
	post_id INT,
	user_id INT,
	content VARCHAR(500),
	datetime_commented DATETIME,
	PRIMARY KEY(id),
	FOREIGN KEY(post_id) REFERENCES posts(id),
	FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE post_likes( 
	id INT, 
	post_id INT, 
	user_id INT, 
	datetime_liked DATETIME, 
	PRIMARY KEY(id), 
	FOREIGN KEY(post_id) REFERENCES posts(id), 
	FOREIGN KEY(user_id) REFERENCES users(id)
);
